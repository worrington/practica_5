<?php

namespace Database\Seeders;

use App\Models\Students;
use Illuminate\Database\Seeder;

class StudentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Students::create([
            'name' => "Montserrat Delgado",
            'code' => 211404197,
            'university' => "Universidad de Guadalaraja",
            'career' => "Ingenieria en Computación",
            'average' => 90.5,
            'email' => "montserrat.delgado@gmail.com",
        ]);
    }
}
